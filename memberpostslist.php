<?php
require 'process.php';
?>

<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Basic 1.1//EN"
    "http://www.w3.org/TR/xhtml-basic/xhtml-basic11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Member Posts List</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
</head>
<body>
    <header class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <a href="index.php" class="navbar-brand mb-0 h1">TIME BANK</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="index.php">HOME</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="memberlists.php">SERVICE POSTS</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="index.php">LOGOUT</a>
                    </li>
                </ul>
            </div>
        </div>
    </header>

    <?php
    if (isset($_SESSION['message'])); ?>

    <div class="alert alert-<?=$_SESSION['msg_type']?>">

        <?php
        echo $_SESSION['message'];
        unset($_SESSION['message']);
        ?>
    </div>

    <div class="container">
    <?php
    $conn = mysqli_connect("localhost", "root", "", "ha07");
    $sql = "SELECT * FROM member_post";
    // pre_r ($result);
    ?>

    <div class="row justify-content-center">
        <h1>Member Posts</h1>
        <p>Below is the lists of the member posts.</p>
        <table class="table table-striped table-bordered mydatatable">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Name</th>
                    <th scope="col">Type</th>
                    <th scope="col">Skill</th>
                    <th scope="col">Location</th>
                    <th scope="col">Status</th>
                    <th scope="col" colspan="2">Action</th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <th scope="col"></th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                    <th scope="col" colspan="2"></th>
                </tr>
            </tbody>
            
            <?php
            while ($row = $result->fetch_assoc()): ?>
                <tr>
                    <td><?php echo $row['Id']; ?></td>
                    <td><?php echo $row['Name']; ?></td>
                    <td><?php echo $row['Type']; ?></td>
                    <td><?php echo $row['Skill']; ?></td>
                    <td><?php echo $row['Location']; ?></td>
                    <td><?php echo $row['Status']; ?></td>
                    <td><?php echo $row['Action']; ?></td>
                    <td>
                        <a href="memberlists.php?edit=<?php echo $row['Id']; ?>"
                            class="btn btn-info">Edit</a>
                        <a href="process.php?delete=<?php echo $row['Id']; ?>"
                            class="btn btn-danger">Delete</a>
                    </td>
                </tr>
            <?php endwhile; ?>
        </table>
    </div>

    <?php
    function pre_r( $array )
    {
        echo '<pre>';
        print_r($array);
        echo '</pre>';
    }
    ?>

    <div class="jumbotron text-center">
        <p>Copyright 2019 Time Banking Management System</p>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script>
        $('.mydatatable').DataTable();
    </script>
    <script>
    // the name of the file appear on select
    $(".photo-input").on("change", function()
    {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".photo-label").addClass("selected").html(fileName);
    });
    </script>

</body>
</html>
