<?php
session_start();

$captcha_num = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890abcdefghijklmnopqrstuvwxyz';
$captcha_num = substr(str_shuffle($captcha_num), 0, 6);
$_SESSION["code"] = $captcha_num;

$img_height = 40; // captcha image height
$img_width = 70; // captcha image width
$font_size = 30; // captcha font size

header('Content-type: image/jpeg');
$image = imagecreate(100, 50); // create background image with dimensions
$bg = imagecolorallocate($image, 255, 255, 255); // set background color
$textcolor = imagecolorallocate($image, 30, 30, 30); // set captcha text color

imagettftext($image, 30, 0, 15, 30, $textcolor, 'font.ttf', $captcha_num);
imagejpeg($image);
?>
