<?php
require 'process.php';
?>

<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Basic 1.1//EN"
    "http://www.w3.org/TR/xhtml-basic/xhtml-basic11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Member's Posts</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
    <header class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <a href="index.php" class="navbar-brand mb-0 h1">TIME BANK</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="index.php">HOME</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="memberlists.php">SERVICE POSTS</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="index.php">LOGOUT</a>
                    </li>
                </ul>
            </div>
        </div>
    </header>

    <div class="container">
        <div class="col-xs-12">
            <form action="addmemberposts.php" method="post">
                <h1>Add New Post</h1>
                <p>Please fill in all the informations below.</p>

                <div class="form-group">
                    <label for="name"><b>Full Name</b></label>
                    <input type="text" class="form-control" id="Name" required>
                </div>

                <div class="form-group">
                    <label for="servicetype"><b>Service type</b></label>
                    <select class="custom-select" id="Type" required>
                        <option selected>Please Select</option>
                        <option value="Plumbing">Plumbing</option>
                        <option value="Accounting">Accounting</option>
                        <option value="Baby-sitting">Baby-sitting</option>
                        <option value="Designing">Designing</option>
                        <option value="Programming">Programming</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="status"><b>Status</b></label>
                    <select class="custom-select" id="Status" required>
                        <option selected>Please Select</option>
                        <option value="Open">Open</option>
                        <option value="Completed">Completed</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="skillsrequired"><b>Skills required</b></label>
                    <select class="custom-select" id="Skills_Required" required>
                        <option selected>Please Select</option>
                        <option value="Maths">Maths</option>
                        <option value="English">English</option>
                        <option value="Drawing">Drawing</option>
                        <option value="Writing">Writing</option>
                        <option value="Reading">Reading</option>
                        <option value="None">None</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="servicelocation"><b>Service's location</b></label>
                    <select class="custom-select" id="Location" required>
                        <option selected>Please Select</option>
                        <option value="Alor Setar">Alor Setar</option>
                        <option value="George Town">George Town</option>
                        <option value="Ipoh">Ipoh</option>
                        <option value="Johor Bahru">Johor Bahru</option>
                        <option value="Kangar">Kangar</option>
                        <option value="Kota Bharu">Kota Bharu</option>
                        <option value="Kota Kinabalu">Kota Kinabalu</option>
                        <option value="Kuala Lumpur">Kuala Lumpur</option>
                        <option value="Kuala Terengganu">Kuala Terengganu</option>
                        <option value="Kuantan">Kuantan</option>
                        <option value="Kuching">Kuching</option>
                        <option value="Labuan">Labuan</option>
                        <option value="Malacca City">Malacca City</option>
                        <option value="Putrajaya">Putrajaya</option>
                        <option value="Seremban">Seremban</option>
                        <option value="Shah Alam">Shah Alam</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="credit"><b>Credit</b></label>
                    <input type="number" class="form-control" id="Credit" required>
                </div>

                <div class="form-group">
                    <label for="photo"><b>Photo</b></label>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="Image">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                        <br><br>
                        <button type="submit" class="btn btn-success btn-block" id="create">Create</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="jumbotron text-center">
        <p>Copyright 2019 Time Banking Management System</p>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script>
    // the name of the file appear on select
    $(".photo-input").on("change", function()
    {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".photo-label").addClass("selected").html(fileName);
    });
    </script>

</body>
</html>
