<?php
require 'memberlogin.php';
require 'registeraccount.php';
?>

<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Basic 1.1//EN"
    "http://www.w3.org/TR/xhtml-basic/xhtml-basic11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Verify Your Account</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
    <header class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <a href="index.php" class="navbar-brand mb-0 h1">TIME BANK</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
    </header>

    <div class="container">
        <div class="card card-body">
            <form action="login.php" method="post" class="needs-validation" novalidate>
                <h1 class="text-center">Account Verification</h1>

                <div class="form-group">
                    <label for="email"><b>Email</b></label>
                    <input type="email" class="form-control" id="Email" required>
                </div>

                <div class="form-group">
                    <label for="code"><b>Activation Code</b></label>
                    <input type="text" class="form-control" id="Activation_Code" required>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-block" id="submit">Verify my account</button>
                </div>
            </form>
        </div>
    </div>

    <div class="jumbotron text-center">
        <p>Copyright 2019 Time Banking Management System</p>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    
</body>
</html>
