<?php
require 'connection.php';

if (isset($_POST['submit']))
{
    $Username = $_POST['Username'];
    $Password = $_POST['Password'];
    $Email = $_POST['Email'];
    $Captcha = $_POST['Captcha'];
    $Skills = $_POST['Skills'];

    $sql = "INSERT INTO member_account (Username, Password, Email, Captcha, Skills) VALUES ('$Username', '$Password', '$Email', '$Captcha', '$Skills')" ;
    
    if (mysqli_query($conn, $sql))
    {
        echo "New record has been saved successfully!";
    }
    else
    {
        echo "Error: " . $sql . "<br>" . mysqli_connect_error();
    }
    mysqli_close($conn);

    header("location: registeraccount.php");
}

if (isset($_POST['create']))
{
    $Name = $_POST['Name'];
    $Type = $_POST['Type'];
    $Status = $_POST['Status'];
    $Skills_Required = $_POST['Skills_Required'];
    $Location = $_POST['Location'];
    $Credit = $_POST['Credit'];
    $Image = $_POST['Image'];

    $sql = "INSERT INTO member_post (Name, Type, Status, Skills_Required, Location, Credit, Image) VALUES ('$Name', '$Type', '$Status', '$Skills_Required', '$Location', '$Credit', '$Image')";
    
    if (mysqli_query($conn, $sql))
    {
        echo "New record has been saved successfully!";
    }
    else
    {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }
    mysqli_close($conn);

    header("location: addmemberposts.php");
}

if (isset($_GET['delete']))
{
    $Id = $_GET['delete'];
    $sql = ("DELETE * FROM member_post WHERE Id = $Id");

    if (mysqli_query($conn, $sql))
    {
        echo "Record has been deleted successfully!";
    }
    else
    {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }
    mysqli_close($conn);

    header("location: memberpostslist.php");
}

if (isset($_GET['update']))
{
    $Id = $_POST['Id'];
    $Name = $_POST['Name'];
    $Type = $_POST['Type'];
    $Status = $_POST['Status'];
    $Skills_Required = $_POST['Skills_Required'];
    $Location = $_POST['Location'];
    $Credit = $_POST['Credit'];
    $Image = $_POST['Image'];

    $sql = ("UPDATE * FROM member_post WHERE Id = $Id");

    if (mysqli_query($conn, $sql))
    {
        echo "Record has been updated successfully.";
    }
    else
    {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }
    mysqli_close($conn);

    header("location: editmemberposts.php");
}
?>
