<?php

$message = "";

$conn = mysqli_connect("localhost", "root", "", "ha07");

// if member click on the Login button
if(isset($_POST["login"]))
{
    // receive all the input values in this form
    $Username = $_POST(['Username']);
    $Password = md5($Password); // password encrypted

    $query1 = "SELECT * member_account WHERE Username = '$Username' and Password = '$password'";
    $result = mysql_query($conn, $query1);
    $result1 = mysqli_num_rows($result);

    if ($result1 =true)
    {
        $Username = $_SESSION['Username'];
        header ('location: index.php');
    }
}

$errors = array();
$Username = "";
$Password = "";

// to validate whether isn't a member or not
if (empty($Username))
{
    $errors['Username'] = "Username is required";
}
if (empty($Password))
{
    $errors['Password'] = "Password is required";
}

// for login member if there are no errors in this form
if (count($errors) == 0)
{
    $Password = md5($Password); // encrypt the password before saving to the database
    $verified = false;
    $sql = "SELECT * FROM member_account WHERE username = '$username' AND password = '$password'";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param($Username, $Password)
    $stmt->execute();
    $result = $stmt->get_result();

    if ($stmt->execute())
    {
        $id = $conn->insert_id;
        $_SESSION['Id'] = $Id;
        $_SESSION['Username'] = $Username;
        $_SESSION['Password'] = $Password;
        $_SESSION['verified'] = $verified;
        $_SESSION['message'] = "You are now logged in successfully";
        $_SESSION['alert-class'] = 'alert-success';
        header('location: verifymember.php');
    }
    else
    {
        $errors = "Both Username or Password does not matched!";
    }
}
?>

<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Basic 1.1//EN"
    "http://www.w3.org/TR/xhtml-basic/xhtml-basic11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Sign In</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
    <header class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <a href="index.php" class="navbar-brand mb-0 h1">TIME BANK</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="index.php">HOME</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">SERVICE POSTS</a>
                    </li>
                </ul>
            </div>
        </div>
    </header>

    <?php echo $message; ?>

    <div class="container">
        <div class="card card-body">
            <form action="login.php" method="post" class="needs-validation" novalidate>
                <h1 class="text-center">Member Sign In</h1>

                <div class="form-group">
                    <label for="username"><b>Username</b></label>
                    <input type="text" class="form-control" id="Username" placeholder="Enter Your Username" required>
                    <div class="invalid-feedback"><b>Please provide a valid username!</b></div>
                </div>

                <div class="form-group">
                    <label for="password"><b>Password</b></label>
                    <input type="password" class="form-control" id="Password" placeholder="Enter Your Password" required>
                    <div class="invalid-feedback"><b>Please provide a valid password!</b></div>
                </div>

                <div class="form-group">
                    <button type="submit" id="login" class="btn btn-primary btn-block">Login</button><br>
                    <p class="text-center">Not a member yet? <a href="registeraccount.php">Sign Up Here</a></p>
                </div>
            </form>
        </div>
    </div>

    <div class="jumbotron text-center">
        <p>Copyright 2019 Time Banking Management System</p>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script>
    // Disable form submissions if there are invalid fields
    (function() {
        'use strict';
        window.addEventListener('load', function() {
        // Get the forms we want to add validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false)
                {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
    })();
    </script>

</body>
</html>
