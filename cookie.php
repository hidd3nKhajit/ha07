<?php
session_start();

// Setting a cookie
setcookie("Username", "", time()+30*24*60*60);

// Accessing an individual cookie value
echo $_COOKIE["Username"];

// Verifying whether a cookie is set or not
if(isset($_COOKIE["Username"]))
{
	echo "Hi " . $_COOKIE["Username"];
}
else
{
	echo "Welcome Guest!";
}
?>
