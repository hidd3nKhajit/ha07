<?php
require 'connection.php';
require 'phpmailer/PHPMailerAutoload.php';
require 'credential.php';

$message = "";

// Captcha validation
if(isset($_POST) & !empty($_POST))
{
    if($_POST['Captcha'] == $_SESSION['Captcha']
    {
        echo "Correct Captcha";
    }
    else
    {
        echo "Invalid Captcha";
    }
}

// alert message for register member account
if(isset($_POST["submit"]))
{
    $query = "SELECT * member_account (Username, Password, Email, Captcha, Skills) VALUES (?,?,?,?,?)";
}
$statement = $connect->prepare($query);
if($statement->execute($user_data))
{
    $message = '<div class="alert alert-success" role="alert">
    Your account registration completed successfully!</div>';
}
else
{
    $message = '<div class="alert alert-danger" role="alert">
    There is an error in your account registration!</div>';
}

$conn = mysqli_connect("localhost", "root", "", "ha07");

$verificationcode = rand('10000', '99999');
// echo $verificationcode;
$_SESSION["random"] = $verificationcode;

// PHPMailer for account verification
if (isset($_POST['Email']))
{
    $mail = new PHPMailer;

    $mail->Host = 'smtp.gmail.com';         // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                 // Enable SMTP authentication
    $mail->SMTPSecure = 'tls';              // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                      // TCP port to connect to
    $mail->Username = EMAIL;                // SMTP username
    $mail->Password = PASS;                 // SMTP password

    $mail->setFrom('');
    $mail->addAddress(EMAIL);               // Add a recipient
    $mail->addReplyTo('');

    $mail->isHTML(true);                    // Set email format to HTML
    $mail->Subject = 'Time Banking Registration Email Confirmation';
    $mail->Body    = 'Thank you for registering with the Time Banking. This is your new activation code';
    $mail->AltBody = '';

    if(!$mail->send())
    {
        echo 'Message could not be sent!';
    }
    else
    {
        echo 'Message has been sent.';
    }
}
?>

<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Basic 1.1//EN"
    "http://www.w3.org/TR/xhtml-basic/xhtml-basic11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Register Your Account</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
    <header class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <a href="index.php" class="navbar-brand mb-0 h1">TIME BANK</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="index.php">HOME</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">SERVICE POSTS</a>
                    </li>
                </ul>
            </div>
        </div>
    </header>

    <?php echo $message; ?>

    <div class="container">
        <div class="col-xs-12">
            <form action="process.php" method="post" class="needs-validation" novalidate>
                <h1>Account Registration</h1>
                <p>Please fill in all the informations below.</p>

                <div class="form-group">
                    <label for="username"><b>Username</b></label>
                    <input type="text" class="form-control" id="Username" placeholder="Enter Your Username" required>
                    <div class="invalid-feedback"><b>Please provide a valid username!</b></div>
                </div>
                
                <div class="form-group">
                    <label for="password"><b>Password</b></label>
                    <input type="password" class="form-control" id="Password" placeholder="Enter Your Password" required>
                    <div class="invalid-feedback"><b>Please provide a valid password!</b></div>
                </div>

                <div class="form-group">
                    <label for="email"><b>Email Address</b></label>
                    <input type="email" class="form-control" id="Email" placeholder="Enter Your Email Address" required>
                    <div class="invalid-feedback"><b>Please provide a valid email address!</b></div>
                </div>

                <div class="form-group">
                    <img src="captcha.php"><br>
                    <label><b>Please enter the code from the image here</b></label>
                    <input type="text" class="form-control" id="Captcha" required>
                    <div class="invalid-feedback"><b>Please enter a valid captcha!</b></div>
                </div>

                <div class="form-group">
                    <label for="skills"><b>Skills</b></label>
                    <select class="custom-select" id="Skills" required>
                        <option selected></option>
                        <option value="Plumbing">Plumbing</option>
                        <option value="Teaching">Teaching</option>
                        <option value="Programming">Programming</option>
                        <option value="Accounting">Accounting</option>
                        <option value="Cooking">Cooking</option>
                        <option value="Driving">Driving</option>
                        <option value="None">None</option>
                    </select>
                    <div class="invalid-feedback"><b>Please select your skill!</b></div>
                </div>

                <button type="submit" class="btn btn-success btn-block" id="submit">Submit</button>
                <button type="reset" class="btn btn-primary btn-block">Reset</button><br>
            </form>
        </div>
    </div>

    <div class="jumbotron text-center">
        <p>Copyright 2019 Time Banking Management System</p>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script>
    // Disable form submissions if there are invalid fields
    (function() {
        'use strict';
        window.addEventListener('load', function() {
        // Get the forms we want to add validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false)
                {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
    })();
    </script>

</body>
</html>
